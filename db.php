<?php
/* Database Connection */
require_once 'classes/dbconnection/Database.php';
require_once 'classes/dbconnection/QueryClass.php';

/*******  *******/
/** Base Classes **/
require_once 'classes/base/Country.php';
require_once 'classes/base/Cases.php';
require_once 'classes/base/Users.php';
require_once 'classes/base/Covid.php';
require_once 'classes/base/Menu.php';




/** Use case **/
use DBConnection\Database;

use Base\Country;
use Base\Cases;
use Base\Covid;
use Base\Users;
use Base\Menu;




$db = new Database();
$pdo = $db->connect('mysql','localhost','covidtracker','root','');
$countryObj = new Country($pdo);
$casesObj = new Cases($pdo);
$user = new Users($pdo);
$covidObj = new Covid($pdo);
$menuObj = new Menu($pdo);

?>