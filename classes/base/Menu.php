<?php
namespace Base;

use DBConnection\Query;

class Menu extends Query{

    public function selectAll(){
        $stmt = $this->pdo->prepare("SELECT * FROM menu");
        $stmt->execute(); 
        $row =  $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $row;
    }

    public function getMenuBasedOnId($id){
        $stmt = $this->pdo->prepare("SELECT * FROM menu WHERE id = :id");
        $stmt->execute(['id' => $id]); 
        $row =  $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }

    public function updateMenu($id,$menuname, $menulink){
        $stmt = $this->pdo->prepare("UPDATE menu
        SET menu_title = :menuname, menu_link = :menulink
        WHERE id = :id");
        $stmt->execute(['id' => $id , 'menuname' =>$menuname, 'menulink' => $menulink] );   

        header('Location: ../../views/admin/pages/menu.php');
     
    }


    public function deleteMenu($id){
        $stmt = $this->pdo->prepare("DELETE FROM menu
        WHERE id = :id");
        $stmt->execute(['id' => $id ]);   
        header('Location: ../../admin/pages/menu.php');
    }


    
}