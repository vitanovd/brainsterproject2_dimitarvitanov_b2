<?php
namespace Base;

use DBConnection\Query;

class Covid extends Query{

    public function selectAll(){
        $stmt = $this->pdo->prepare("SELECT * FROM covid");
        $stmt->execute(); 
        $row =  $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $row;
    }


    public function deleteCovid($id){
        $stmt = $this->pdo->prepare("DELETE FROM covid
        WHERE id = :id");
        $stmt->execute(['id' => $id ]);   
        header('Location: ../../admin/pages/about.php');
    }


    public function updateCovid($id,$aboutitle, $aboutcontent, $articletitle, $articlecontent){
        $stmt = $this->pdo->prepare("UPDATE covid
        SET about_covid_title = :aboutitle, about_covid = :aboutcontent, article_title = :articletitle, article_content = :articlecontent
        WHERE id = :id");
        $stmt->execute(['id' => $id , 'aboutitle' =>$aboutitle, 'aboutcontent' => $aboutcontent, 'articletitle' => $articletitle, 'articlecontent' => $articlecontent ] );   
        header('Location: ../../views/admin/pages/about.php');
    }

    public function getCovidBasedOnId($id){
        $stmt = $this->pdo->prepare("SELECT * FROM covid WHERE id = :id");
        $stmt->execute(['id' => $id]); 
        $row =  $stmt->fetch(\PDO::FETCH_OBJ);
        return $row;
    }


    
}