<?php 

namespace Base;
use DBConnection\Query;

class Users extends Query{
    public function selectAllUsers(){
        $sql = 'SELECT * FROM users';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $row;
     }
     public function checkUser($username, $password){
            $users = $this->selectAllUsers();
            foreach($users as $user){
                if($user->username == $username && $user->password == $password){
                    session_start();
                    $_SESSION['user'] = $username;
                    header('Location: ../../views/admin/admin.php');
                }
                else{
                    header("Location: ../../views/admin/login.php?error='true'");
                }
            }
     }
}