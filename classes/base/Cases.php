<?php

namespace Base;

use DBConnection\Query;


class Cases extends Query
{

    public function update()
    {
        $apiReturn = '';
        //$date = Last date data for each country
        $data = $this->getLastDate();
        $currentDate = date('Y-m-d', strtotime("-1 days"));
        $j = 1;
        foreach ($data as $country) {
            $date = strtotime($country->date);
            // 36400
            $lastDateFromDb = date('Y-m-d', $date + 84600);
            if ($lastDateFromDb >= $currentDate) {
            } else {
                if ($j == 10) {
                    $j = 1;
                    set_time_limit(20000);
                    sleep(60);
                }
                $handle = curl_init();
                $url = "https://api.covid19api.com/total/country/$country->slug?from=$lastDateFromDb&to=$currentDate";
                curl_setopt($handle, CURLOPT_URL, $url);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($handle);
                curl_close($handle);
                $newCases = json_decode($result, true);

                if(!$newCases){
                    if($apiReturn == ''){
                    $apiReturn = 'Api Down';
                    echo 'Api down !' . "</br>";
                    die("<a href='../../views/admin/admin.php'>Refresh adminpanel </a>");
                    }
                      
                }else{

               
                foreach ($newCases as $key => $case) {
                    $caseDatearr = explode('T', $case['Date']);
                    $caseDate = $caseDatearr[0];
                    if ($this->dateExists($caseDate, $country->id) == false) {
                        $newCase = [
                            'countryId' => $country->id,
                            'confirmed' => $case['Confirmed'],
                            'deaths' => $case['Deaths'],
                            'recovered' => $case['Recovered'],
                            'active' => $case['Active'],
                            'date' => $caseDate,
                        ];
                        if ($key == 0) {
                            $newCase['confirmedToday'] = $case['Confirmed']  - (int)$country->confirmed;
                            $newCase['deathsToday'] = $case['Deaths'] -  (int)$country->deaths;
                            $newCase['recoveredToday'] = $case['Recovered'] -  (int)$country->recovered;
                            $this->insertData($newCase);
                            set_time_limit(1000000000000);
                            sleep(1);
                        } else if ($key > 0) {
                            $prevIndex = $newCases[$key - 1];
                            $newCase['confirmedToday'] = $case['Confirmed'] - $prevIndex['Confirmed'];
                            $newCase['deathsToday'] = $case['Deaths'] - $prevIndex['Deaths'];
                            $newCase['recoveredToday'] = $case['Recovered'] - $prevIndex['Recovered'];
                            $this->insertData($newCase);
                            set_time_limit(1000000000000);
                            sleep(1);
                        }
                    } else {
                    }
                }
                 }
            }
            $j++;
        }
    }


    public function getLastDate()
    {

        $sql = "SELECT  countries.id,countries.slug,cases.date,cases.confirmed ,cases.deaths,cases.recovered FROM cases 
        JOIN countries ON cases.country_id=countries.id
         JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a 
         ON cases.country_id = a.country_id and cases.date = a.date";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }





    public function dateExists($date, $countryId)
    {
        $sql = "SELECT * FROM cases WHERE date ='$date' AND country_id=$countryId";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return  $data;
    }




    public function insertData(array $newCase)
    {

        $sql = "INSERT INTO cases(country_id, confirmed, deaths, recovered,active, date, confirmed_today, death_today, recovered_today)
        VALUES({$newCase['countryId']},
        {$newCase['confirmed']},
        {$newCase['deaths']},
        {$newCase['recovered']},
        {$newCase['active']},
        '{$newCase['date']}',
        {$newCase['confirmedToday']},
        {$newCase['deathsToday']},
        {$newCase['recoveredToday']}
        )";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }

  
    public function totalCasesPerCountry()
    {
        $stmt = $this->pdo->prepare("SELECT countries.slug AS 'Slug', MAX(cases.confirmed) as 'ConfirmedCases', MAX(cases.deaths) as 'DeathsCases', MAX(cases.recovered) as 'RecoveredCases'  FROM cases JOIN countries ON cases.country_id = countries.id GROUP BY cases.country_id ");
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }

   

    public function getTotalCases()
    {
        $stmt = $this->pdo->prepare("SELECT cases.date ,SUM(cases.confirmed)as 'TotalCases' FROM 
        cases JOIN countries ON cases.country_id=countries.id
        JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a ON cases.country_id = a.country_id and cases.date = a.date
        ");
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public function getTotalRecovered()
    {
        $stmt = $this->pdo->prepare("SELECT cases.date ,SUM(cases.recovered)as 'TotalCases' FROM 
        cases JOIN countries ON cases.country_id=countries.id
        JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a ON cases.country_id = a.country_id and cases.date = a.date
        ");
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }



    public function getTotalConfirmedBasedOnId($id)
    {
        $stmt = $this->pdo->prepare("SELECT SUM(cases.confirmed_today) as 'TotalCases' FROM cases WHERE country_id = :id");
        $stmt->execute(['id'=>$id]);
        $data = $stmt->fetch();
        return $data;
    }

    public function getTotalRecoveredBasedOnId($id)
    {
        $stmt = $this->pdo->prepare("SELECT SUM(cases.recovered_today) as 'TotalCases' FROM cases WHERE country_id = :id");
        $stmt->execute(['id'=>$id]);
        $data = $stmt->fetch();
        return $data;
    }
    public function getTotalDeathCasesBasedOnId($id)
    {
        $stmt = $this->pdo->prepare("SELECT SUM(cases.death_today) as 'TotalCases' FROM cases WHERE country_id = :id");
        $stmt->execute(['id'=>$id]);
        $data = $stmt->fetch();
        return $data;
    }

    public function getTotalDeaths()
    {
        $stmt = $this->pdo->prepare("SELECT cases.date ,SUM(cases.deaths)as 'TotalCases' FROM 
        cases JOIN countries ON cases.country_id=countries.id
        JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a ON cases.country_id = a.country_id and cases.date = a.date
        ");
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public function getLastUpdatedData()
    {
        $stmt = $this->pdo->prepare("SELECT  countries.id,countries.name,cases.date,confirmed_today ,death_today,recovered_today FROM cases 
        JOIN countries ON cases.country_id=countries.id
         JOIN (SELECT country_id, max(date) as Date FROM cases GROUP BY country_id) as a 
         ON cases.country_id = a.country_id and cases.date = a.date");
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }


    public function getLastDataOnGlobalScale()
    {
        $stmt = $this->pdo->prepare("SELECT countries.id,countries.name,MAX(cases.date)as date,SUM(confirmed_today) as 'confirmed' , SUM(death_today) as 'deaths', SUM(recovered_today) AS 'recovered' FROM cases JOIN countries ON cases.country_id=countries.id GROUP BY country_id ");
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }

    public function getDataBasedOnId($id){
        $stmt = $this->pdo->prepare("SELECT * FROM cases JOIN countries ON cases.country_id = countries.id WHERE country_id = :id ORDER BY date ASC");
        $stmt->execute(['id' => $id]);
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }

    public function getCountryWithMostDeaths(){
        $stmt = $this->pdo->prepare("SELECT countries.name, SUM(cases.death_today) FROM cases JOIN countries ON cases.country_id = countries.id GROUP BY country_id ORDER BY SUM(cases.death_today) DESC LIMIT 1 ");
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_OBJ);
        return $data;
    }


    public function getTotalsMontlyBased($id){
        $stmt = $this->pdo->prepare("SELECT cases.date,countries.name as 'CountryName',sum(confirmed_today) AS 'ConfirmedCases', sum(death_today) AS 'DeathCases', sum(recovered_today) AS 'RecoveredCases', date FROM cases JOIN countries ON cases.country_id = countries.id  WHERE country_id = :id GROUP BY YEAR(date),MONTH(date)");
        $stmt->execute(['id'=>$id]);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }


    public function getCasesBasedOnDate($date){
        $stmt = $this->pdo->prepare("SELECT * FROM cases JOIN countries ON cases.country_id = countries.id WHERE date = :date GROUP BY cases.country_id");
        $stmt->execute(['date' => $date]);
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }


    public function getCasesMonthlyBasedOnDate($date){
        $year =  date('Y', strtotime($date));
        $month =  date('m', strtotime($date));
        $stmt = $this->pdo->prepare("SELECT countries.name, countries.id, date, SUM(confirmed_today) as 'confirmed_today', SUM(death_today) as 'death_today' ,SUM(recovered_today) AS 'recovered_today' FROM cases JOIN countries ON cases.country_id = countries.id where MONTH(cases.date) = $month AND YEAR(cases.date) = $year GROUP BY country_id");
        $stmt->execute(['date' => $date]);
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }


    public function getCasesBasedOnDateAndId($date,$id){
        $stmt = $this->pdo->prepare("SELECT * FROM cases JOIN countries ON cases.country_id = countries.id WHERE date = :date AND cases.country_id = :id GROUP BY cases.country_id");
        $stmt->execute(['date' => $date , 'id' => $id]);
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }

    
    public function getCasesMonthlyBasedOnDatAndId($date,$id){
        $year =  date('Y', strtotime($date));
        $month =  date('m', strtotime($date));
        $stmt = $this->pdo->prepare("SELECT countries.name, countries.id, date, SUM(confirmed_today) as 'confirmed_today', SUM(death_today) as 'death_today' ,SUM(recovered_today) AS 'recovered_today' FROM cases JOIN countries ON cases.country_id = countries.id where MONTH(cases.date) = $month AND YEAR(cases.date) = $year AND cases.country_id=:id GROUP BY country_id");
        $stmt->execute(['id' => $id]);
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }


    public function getCasesThreeMonthsBasedOnDate($date){
        $stmt = $this->pdo->prepare("SELECT countries.name, countries.id, date, SUM(confirmed_today) as 'confirmed_today', SUM(death_today) as 'death_today' ,SUM(recovered_today) AS 'recovered_today' FROM cases JOIN countries ON cases.country_id = countries.id WHERE date BETWEEN DATE_ADD('{$date}', INTERVAL - 3 MONTH) AND '{$date}' GROUP BY cases.country_id
        ");
        $stmt->execute(['date' => $date]);
        $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }
    


    
    
}
