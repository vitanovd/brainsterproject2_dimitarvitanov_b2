<?php
 namespace Base;
 use DBConnection\Query;


 class Country extends Query{

    public function selectAll(){
        $stmt = $this->pdo->prepare("SELECT * FROM countries WHERE isEmptyData = 'false'");
        $stmt->execute(); 
        $row =  $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $row;
    }

    public function selectAllCountries(){
        $stmt = $this->pdo->prepare("SELECT * FROM countries");
        $stmt->execute(); 
        $row =  $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $row;
    }

    public function getCountryName($id){
        $stmt = $this->pdo->prepare("SELECT countries.name FROM countries WHERE id = :id");
        $stmt->execute(['id' => $id]);
        $data = $stmt->fetch(\PDO::FETCH_OBJ);
        return $data;
    }

    public function getCountCountries(){
        $stmt = $this->pdo->prepare("SELECT count(id) as 'numCountries' FROM countries ");
        $stmt->execute(); 
        $row =  $stmt->fetch();
        return $row;
    }
    
    

 }

?>
