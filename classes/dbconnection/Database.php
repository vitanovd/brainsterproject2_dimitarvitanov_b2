<?php
namespace DBConnection;
class Database{
    public function connect($driver, $host, $dbname, $user, $password) : \PDO{
        $dns = "$driver:host=$host;dbname=$dbname";
        try{
             return new \PDO($dns,$user,$password);
        }
        catch(\PDOException $e){
            return $e->getMessage();
        }
    }

}


?>