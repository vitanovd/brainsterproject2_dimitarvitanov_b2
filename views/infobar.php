<div class="container" data-aos="fade-up">
    <div class="row">

        <div class="col-12 info-bar bg-white  ">
        <h4 class="text-center py-3 py-md-0 text-md-center text-uppercase text-muted">W<i class="fas fa-globe"></i>rldwide cases</h4>

            <div class="row justify-content-center">

                <div class="col-md-3 d-flex justify-content-md-center ml-4">
                    <a href="" class="fas fa-virus virus-ic-one mt-3"></a>
                    <div>
                        <h6 class="text-muted text-capitalize ml-2 mt-2">Total Confirmed Cases</h6>
                        <h4 class="ml-2 mt-2"><?php $totlacases = $casesObj->getTotalCases();
                                                echo number_format($totlacases['TotalCases']) ?></h4>
                    </div>

                </div>

                <div class="col-md-4 d-flex  justify-content-md-center my-5 my-md-0 ml-4">
                    <a href="" class="fas fa-virus virus-ic-two mt-3"></a>
                    <div>
                        <h6 class="text-muted text-capitalize ml-2 mt-2">Total recovered cases</h6>
                        <h4 class="ml-2 mt-2"><?php $totlacases = $casesObj->getTotalRecovered();
                                                echo number_format($totlacases['TotalCases']) ?></h4>
                    </div>

                </div>

                <div class="col-md-3 d-flex justify-content-md-center ml-4 ">
                    <a href="" class="fas fa-virus virus-ic-three mt-3"></a>
                    <div>
                        <h6 class="text-muted text-capitalize ml-2 mt-2">Total deaths cases</h6>
                        <h4 class="ml-2 mt-2"><?php $totlacases = $casesObj->getTotalDeaths();
                                                echo number_format($totlacases['TotalCases']) ?></h4>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
