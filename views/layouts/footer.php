<!-- jQuery library -->

<footer>
    <div class="container px-0">
        <div class="row mx-0 ">
            <div class="col-12 col-lg-5 d-flex justify-content-center  flex-column  mt-5">
                <p class="display-3 text-white">COVID - 19</p>
                <small class="mt-4 mt-md-0 text-white text-justify">
                    The human cost of coronavirus has continued to mount, with more than 111.4m cases confirmed globally and more than 2.5m people known to have died.
                    The World Health Organization declared the outbreak a pandemic in March and it has spread to more than 200 countries, with severe public health and economic consequences. This page provides an up-to-date visual narrative of the spread of Covid-19, so please check back regularly because we are refreshing it with new graphics and features as the story evolves.
                </small>
            </div>
            <div class="col-12 col-md-6 col-lg-3 d-flex justify-content-center flex-column text-center mt-5">
                <ul class="navbar-nav mx-md-auto     ml-lg-auto">
                    <h2 class="text-white">Menu</h2>

                    <small class="text-warning text-md-left">Go to home page</small>
                    <li class="nav-item ">
                        <a class="nav-link text-md-left" href="index.php">Home </a>
                    </li>
                    <small class="text-warning text-md-left">Learn how to protect yourself</small>

                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/prevention.html">Covid Protection</a>
                    </li>

                    <small class="text-warning text-md-left">Get onboard with the latest statistics</small>

                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="index.php#covidStatistics">Covid Statistics</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="index.php#statisticsManager">Statistics Manager</a>
                    </li>

                    <small class="text-warning text-md-left">View our Covid Map</small>

                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="index.php#covidMap">Covid Map</a>

                    </li>
                </ul>
            </div>

            <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center flex-column text-center mt-2">
                <form class="form-inline my-2 my-lg-0 ">
                    <input class="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search">
                    <button class="btn  ml-auto ml-md-0 btn-outline-success my-2 my-sm-2 " type="submit">Search</button>
                </form>
                <img src="../../public/img/brainster.png" class="img-fluid mt-3 brainster-logo " alt="">

                <p class="font-italic mt-2 text-lg-left text-white mb-1 mt-3">Copyright &copy; Dimitar Vitanov </p>
                <small class="font-italic text-lg-left text-white">Brainster academy</small>
            </div>




        </div>

    </div>


</footer>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script>
<script src="public/js/dist/loading-bar.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script src='https://cdn.jsdelivr.net/npm/chartist@0.11.4/dist/chartist.min.js'></script>
<script src="public/js/script.js"></script>





<?php
$dataResult = $casesObj->totalCasesPerCountry();

echo "<script>";
echo 'let confirmedCases = ' . json_encode($dataResult) . ';';
echo "</script>"

?>

<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['geochart'],
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY',
    });
    google.charts.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
            ['Country', 'ConfirmedCases','Recovered Cases:'],
            <?php
            for ($i = 0; $i < count($dataResult); $i++) {
                if ($dataResult == null || empty($dataResult)) {

                    echo "['" . ucfirst($dataResult[$i]['Slug']) . "', " . $dataResult[$i]['ConfirmedCases'] .  ',' .  $dataResult[$i]['RecoveredCases'] .   "],";
                    
                }
                echo "['" . ucfirst($dataResult[$i]['Slug']) . "', " . $dataResult[$i]['ConfirmedCases'] .   ',' .  $dataResult[$i]['RecoveredCases']  ."],";
            }

            ?>





        ]);

        var options = {};
        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
        chart.draw(data, options);
    }



    let confirmed = <?php $totlacases = $casesObj->getTotalCases();
                    echo $totlacases['TotalCases'] ?>


    let deaths = <?php $totlacases = $casesObj->getTotalDeaths();
                    echo $totlacases['TotalCases'] ?>

    let recovered = <?php $totlacases = $casesObj->getTotalRecovered();
                    echo $totlacases['TotalCases'] ?>


    var data = {

        series: [confirmed, deaths, recovered]
    };

    var sum = function(a, b) {
        return a + b
    };

    new Chartist.Pie('.ct-chart', data, {

        labelInterpolationFnc: function(value) {
            return Math.round(value / data.series.reduce(sum) * 100) + '%';
        }

    });
</script>
</body>

</html>