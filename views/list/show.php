<?php include '../../db.php' ?>
<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="../../public/css/loading-bar.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../../public/css/style.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link rel="stylesheet" href="cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

</head>

<body>

    <script>
        AOS.init({
            duration: 1500,
            delay: 200
        });
    </script>

    <div class="container-fluid px-0 mx-0">

    <?php include 'components/navbar.php'?>
    
    <?php include 'components/covidstatistics.php'?>

    <?php include 'components/optionpart.php'?>


    
    <footer>

        <div class="container px-0">
            <div class="row mx-0 ">
                <div class="col-12 col-lg-5 d-flex justify-content-center  flex-column  mt-5">
                    <p class="display-3 text-white">COVID - 19</p>
                    <small class="mt-4 mt-md-0 text-white text-justify">
                        The human cost of coronavirus has continued to mount, with more than 111.4m cases confirmed globally and more than 2.5m people known to have died.
                        The World Health Organization declared the outbreak a pandemic in March and it has spread to more than 200 countries, with severe public health and economic consequences. This page provides an up-to-date visual narrative of the spread of Covid-19, so please check back regularly because we are refreshing it with new graphics and features as the story evolves.
                    </small>
                </div>

                <div class="col-12 col-md-6 col-lg-3 d-flex justify-content-center flex-column text-center mt-5">
                <ul class="navbar-nav mx-md-auto     ml-lg-auto">
                    <h2 class="text-white">Menu</h2>

                    <small class="text-warning text-md-left">Go to home page</small>
                    <li class="nav-item ">
                        <a class="nav-link text-md-left" href="../../index.php">Home </a>
                    </li>
                    <small class="text-warning text-md-left">Learn how to protect yourself</small>

                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="#">Covid Protection</a>
                    </li>

                    <small class="text-warning text-md-left">Get onboard with the latest statistics</small>

                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="../../index.php#covidStatistics">Covid Statistics</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="../../index.php#statisticsManager">Statistics Manager</a>
                    </li>

                    <small class="text-warning text-md-left">View our Covid Map</small>

                    <li class="nav-item">
                        <a class="nav-link text-md-left" href="../../index.php#covidMap">Covid Map</a>

                    </li>
                </ul>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center flex-column text-center mt-2">
                    <form class="form-inline my-2 my-lg-0 ">
                        <input class="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search">
                        <button class="btn  ml-auto ml-md-0 btn-outline-success my-2 my-sm-2 " type="submit">Search</button>
                    </form>
                    <img src="../../public/img/brainster.png" class="img-fluid mt-3 brainster-logo " alt="">

                    <p class="font-italic mt-2 text-lg-left text-white mb-1 mt-3">Copyright &copy; Dimitar Vitanov </p>
                    <small class="font-italic text-lg-left text-white">Brainster academy</small>
                </div>
            </div>
        </div>
    </footer>






    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="../../public/js/script.js" type="text/javascript"></script>

    <script>
        $(document).ready(function() {



                    <?php $dataResult = $casesObj->getTotalsMontlyBased($id) ?>
                    let totalDeaths = <?php $totalDeaths = $casesObj->getTotalDeathCasesBasedOnId($id);
                                        echo $totalDeaths['TotalCases'] ?>;
                    let totalConfirmed = <?php $totalConfirmed =  $casesObj->getTotalConfirmedBasedOnId($id);
                                            echo $totalConfirmed['TotalCases'] ?>;
                    let totalRecovered = <?php $totalRecovered =  $casesObj->getTotalRecoveredBasedOnId($id);
                                            echo $totalRecovered['TotalCases'] ?>;

                    var ctx = document.getElementById('chart2').getContext('2d');
                    var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: ['Deaths', 'Confirmed', 'Recovered'],
                                datasets: [{
                                    label: '# COVID-19 Cases',
                                    data: [totalDeaths, totalConfirmed, totalRecovered],
                                    backgroundColor: [
                                        'rgb(217, 83, 79)',
                                        'rgb(91, 192, 222)',
                                        'rgb(92, 184, 92)',
                                    ],
                                    borderColor: [
                                        'rgb(217, 83, 79)',
                                        'rgb(91, 192, 222)',
                                        'rgb(92, 184, 92)',
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Total Deaths,Confirmed, Recovered',
                                },
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }

                                }
                            });



                        new Chart(document.getElementById("lineChart"), {
                            type: 'line',
                            data: {
                                labels: [
                                    <?php
                                    for ($i = 0; $i < count($dataResult); $i++) {
                                        $months= (string)(date('M-Y', strtotime($dataResult[$i]['date'])));
                                        echo  "'$months'".',';                                    }
                                    ?>
                                ],
                                datasets: [{
                                        data: [
                                            <?php
                                            for ($i = 0; $i < count($dataResult); $i++) {
                                                echo ($dataResult[$i]['ConfirmedCases']) . ',';
                                            }
                                            ?>

                                        ],
                                        borderColor: "#3e95cd",
                                        fill: false,
                                        label: 'Year based confirmed cases'
                                    },

                                    {
                                        data: [
                                            <?php
                                            for ($i = 0; $i < count($dataResult); $i++) {
                                                echo ($dataResult[$i]['DeathCases']) . ',';
                                            }
                                            ?>

                                        ],
                                        borderColor: "rgb(217, 83, 79)",
                                        fill: false,
                                        label: 'Year based death cases'


                                    },


                                    {
                                        data: [
                                            <?php
                                            for ($i = 0; $i < count($dataResult); $i++) {
                                                echo ($dataResult[$i]['RecoveredCases']) . ',';
                                            }
                                            ?>

                                        ],
                                        borderColor: "rgb(92, 184, 92)",
                                        fill: false,
                                        label: 'Year based recovered cases'

                                    }
                                ]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Montly Based Statistics'
                                }
                            }
                        });



                    });
    </script>
</body>

</html>







</body>

</html>