
        <div class="row px-0 mx-0">
            <div class="col-12 px-0">

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark" data-aos="zoom-in">
                    <button class="navbar-toggler ml-auto " type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon  "></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarNav">
                        <a href="#" class="navbar-brand"><img src="../../public/img/logo.png" class="img-fluid logo float-right" alt=""></a>

                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link text-white" href="../../index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">Covid Protection</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="../../index.php#statisticsManager">Covid Statistics</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">Covid Map</a>

                            </li>

                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white font-weight-bold" href="../../views/admin/login.php">Login</a>

                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
