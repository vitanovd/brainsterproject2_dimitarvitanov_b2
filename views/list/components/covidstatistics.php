<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
?>
    
    <div class="container py-2" data-aos="zoom-in">
        <h2 class='text-center py-2 py-md-5 '> <?php $countryName = $countryObj->getCountryName($id);
                                                echo $countryName->name; ?> Statistics </h2>
        <div class="row">
            <div class="col-12 col-lg-8  mt-5  ">

                <table id="tableToday" class="display table-responsive">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th class="bg-info">Confirmed</th>
                            <th class="bg-success">Recovered</th>
                            <th class="bg-danger">Deaths</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($casesObj->getDataBasedOnId($id) as $case) {   ?>

                            <tr>
                                <td><?= $case->date ?></td>
                                <td><?= $case->confirmed ?></td>
                                <td><?= $case->recovered ?></td>
                                <td><?= $case->deaths ?></td>


                            </tr>

                        <?php } ?>
                    </tbody>
                </table>

            </div>

            <div class="col-12 col-lg-4   mt-2 ">

                <!-- <div class="ct-chart ct-golden-section " id="chart2"></div>  -->
                    <canvas id="chart2" width="400" height="400"></canvas>

                        <canvas id="lineChart" class="mt-2" width="400" height="400"></canvas>

            </div>

        </div>
    </div>