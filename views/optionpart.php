
<div class="container option-part"  id="statisticsManager" >
    <div class="row" >
        <div class="col-12" >
            <div class="display-4 text-center py-5" >Feel free to manage our statistics based on your preferences</div>

            <div class="row justify-content-center" >


                <div class="col-12 col-lg-4 offset-lg-1" >

                <?php if (isset($_GET['datePicker'])) {
                        $date = $_GET['datePicker'];
                        $alt = $_GET['alternativePicker'];
                    } else {
                        $date = date(date('Y-m-d', strtotime("-1 days")));
                        $alt = 'daily';
                    }
                    ?>
                    <form action="index.php#statisticsManager" class="statForm" method="GET" >
                        <div class="form-group mt-4">

                            <label for="">Please choose date</label>
                            <input type="date" class="form-control" name="datePicker" id="datePicker" value = "<?= $date?>">


                        </div>


                        <div class="form-group mt-4">
                            <label for="alternativePicker">Choose one of the alternatives</label>
                            <select name="alternativePicker" id="alternativePicker" class="form-control">
                                <option value="daily" selected>Daily</option>
                                <option value="monthly">Monthly</option>
                                <option value="threeMonths">Three Monts</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success float-right">Enter</button>
                    </form>

                
                
                </div>

                <div class="col-12 col-lg-7" >
                 
                    <table id="tableToday3" class="display table-responsive" >
                        <thead>
                            <tr>
                                <th>Country</th>
                                <th>Confirmed</th>
                                <th>Recovered</th>
                                <th>Deaths</th>
                                <th>Date </th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            if ($alt == 'daily') {
                                $resultSet = $casesObj->getCasesBasedOnDate($date);
                                if ($resultSet != null) {
                                    foreach ($resultSet as $data) { ?>
                                        <tr>
                                        <td><a href="../views/list/show.php?id=<?=$data->id ?>" class="text-muted"><?= $data->name ?>   </a></td>
                                            <td><?= $data->confirmed_today ?></td>
                                            <td><?= $data->recovered_today ?></td>
                                            <td><?= $data->death_today?></td>
                                            <td><?= date('Y-M',strtotime($data->date)) ?></td>

                                        </tr>
                            <?php }
                                }
                            } ?>



                            <?php
                            if ($alt == 'monthly') {
                                $resultSet = $casesObj->getCasesMonthlyBasedOnDate($date);
                                if ($resultSet != null) {
                                    foreach ($resultSet as $data) { 
                                        ?>
                                        <tr>
                                        <td><a href="../views/list/show.php?id=<?= $data->id ?>" class="text-muted"><?= $data->name ?>   </a></td>
                                            <td><?= $data->confirmed_today ?></td>
                                            <td><?= $data->recovered_today ?></td>
                                            <td><?= $data->death_today ?></td>
                                            <td><?= date('Y-M',strtotime($data->date)) ?></td>
                                        </tr>
                            <?php }
                                }
                            } ?>


<?php
                            if ($alt == 'threeMonths') {
                                $resultSet = $casesObj->getCasesThreeMonthsBasedOnDate($date);
                                if ($resultSet != null) {
                                    foreach ($resultSet as $data) { 
                                        ?>
                                        <tr>
                                        <td><a href="../views/list/show.php?id=<?= $data->id ?>" class="text-muted"><?= $data->name ?>   </a></td>
                                            <td><?= $data->confirmed_today ?></td>
                                            <td><?= $data->recovered_today ?></td>
                                            <td><?= $data->death_today ?></td>
                                            <td><?= date('Y-M',strtotime($data->date)) ?></td>
                                        </tr>
                            <?php }
                                }
                            } ?>







                     





                        </tbody>
                    </table>
                </div>
            </div> <!-- Row-->
        </div> <!-- Column -->
    </div> <!-- Row-->
</div> <!-- Container-->
