<?php
session_start();

if (!isset($_SESSION['user']) && !isset($_SESSION['password'])) {
    header('Location:../../../views/admin/login.php');
} else {
?>

    <?php include '../components/header.php';
    require '../components/nav.php';
    require '../../../db.php';
    ?>



    <div class="col-12 col-lg-8 py-2 mx-auto">
        <div class="d-flex justify-content-between mt-4">
            <h4 class="">General Information </h4>
            <div class="text-right">
                <a class="text-dark  " href="../../../views/admin/logout.php"><i class="fas fa-sign-out-alt mr-2"> </i> Logout |</a>
                <?php echo date('Y/m/d'); ?>
            </div>
        </div>
        <span class="border border-secondary d-block mt-2"></span>

        <h4 class="mt-4">Total cases worldwide </h4>

        <?php include '../components/admininfobar.php' ?>

        <div class="col-12 mt-3">
            <table  id="tableToday" class="display table-responsive">
                <thead>
                    <th>Country Slug</th>
                    <th>Confirmed Cases</th>
                    <th>Death Cases </th>
                    <th>Recovered Cases </th>

                </thead>

                <tbody>
                    <?php $casesData =  $casesObj->totalCasesPerCountry(); ?>
                    <?php foreach($casesData as $data ){ ?>
                    <tr>
                        <td><?= ucfirst($data['Slug']) ?></td>
                        <td><?= $data['ConfirmedCases'] ?></td>
                        <td><?= $data['DeathsCases'] ?></td>
                        <td><?= $data['RecoveredCases'] ?></td>

                    </tr>

                        <?php }?>
                </tbody>
            </table>
        </div>
    </div>





    </div>

    </div> <!-- Left Admin Side -->








<?php } ?>

<?php require '../components/footer.php' ?>