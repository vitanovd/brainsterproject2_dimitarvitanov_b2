<?php
session_start();

if (!isset($_SESSION['user']) && !isset($_SESSION['password'])) {
    header('Location:../../../views/admin/login.php');
} else {
?>

    <?php include '../components/header.php';
    require '../components/nav.php';
    require '../../../db.php';
    ?>



    <div class="col-12 col-lg-8 py-2 mx-auto">
        <div class="d-flex justify-content-between mt-4">
            <h4 class="">General Information </h4>
            <div class="text-right">
                <a class="text-dark  " href="../../../views/admin/logout.php"><i class="fas fa-sign-out-alt mr-2"> </i> Logout |</a>
                <?php echo date('Y/m/d'); ?>
            </div>
        </div>
        <span class="border border-secondary d-block mt-2"></span>

        <h4 class="mt-4">Total cases worldwide </h4>

        <?php include '../components/admininfobar.php' ?>


        <div class="col-12 ">
            <table class="table table-responsive">
                <thead>
                    <th>About Title</th>
                    <th>About Content</th>
                    <th>Article Title</th>
                    <th>Article Content</th>
                    <th colspan="2" class="text-center">Functionalities</th>
                </thead>

                <tbody>
                    <?php $covidData =  $covidObj->selectAll(); ?>
                    <?php foreach($covidData as $data ){ ?>
                    <tr>
                        <td><?= $data->about_covid_title ?></td>
                        <td><?= $data->about_covid ?></td>
                        <td><?= $data->article_title ?></td>
                        <td><?= $data->article_content ?></td>
                        <td><a href="aboutedit.php?id=<?= $data->id ?>" class="btn btn-warning">Edit</a> </td>
                        <td> <a href="aboutdelete.php?id=<?= $data->id ?>" class="btn btn-danger">Delete</a></td>
                        
                    </tr>

                        <?php }?>
                </tbody>
            </table>
        </div>
    </div>





    </div>

    </div> <!-- Left Admin Side -->








<?php } ?>

<?php require '../components/footer.php' ?>