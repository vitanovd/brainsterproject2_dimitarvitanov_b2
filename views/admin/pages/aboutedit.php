<?php
session_start();

if (!isset($_SESSION['user']) && !isset($_SESSION['password'])) {
    header('Location:../../../views/admin/login.php');
} else {
?>

    <?php include '../components/header.php';
    require '../components/nav.php';
    require '../../../db.php';
    ?>



    <div class="col-12 col-lg-8 py-2 mx-auto">
        <div class="d-flex justify-content-between mt-4">
            <h4 class="">General Information </h4>
            <div class="text-right">
                <a class="text-dark  " href="../../../views/admin/logout.php"><i class="fas fa-sign-out-alt mr-2"> </i> Logout |</a>
                <?php echo date('Y/m/d'); ?>
            </div>
        </div>
        <span class="border border-secondary d-block mt-2"></span>

        <h4 class="mt-4">Total cases worldwide </h4>

        <?php include '../components/admininfobar.php'?>


    <?php if(isset($_GET['id'])){
        $id = $_GET['id']; } 
           $covidData =  $covidObj->getCovidBasedOnId($id);
        ?>
    
        <div class="row">
            <div class="col-12 col-xl-6">
            <h4 class="my-3 text-center text-lg-left">Update current menu information</h4>
                    <form action="../../../public/validation/updateabout.php" method="post">
                        <label for="aboutitle">Update about  title: </label>
                        <input type="text" name="aboutitle" id="aboutitle" class="form-control" value = "<?= $covidData->about_covid_title?>">
                        <label for="aboutcontent">Update about content </label>
                        <textarea name="aboutcontent" class="form-control" id="aboutcontent" cols="30" rows="5"><?=$covidData->about_covid;?>
                        </textarea>

                        <label for="articletitle">Update article  title: </label>
                        <input type="text" name="articletitle" id="articletitle" class="form-control" value = "<?= $covidData->article_title?>">
                        <label for="articlecontent">Update article content </label>
                        <textarea name="articlecontent" class="form-control" id="articlecontent" cols="30" rows="5"><?=$covidData->article_content;?></textarea>


                        <input type="hidden" name="id" value="<?= $covidData->id?>">
                        <button type="submit" class="btn btn-warning float-right mt-2">Update</button>
                    </form>
            </div>

        </div>





        </div>

    </div> <!-- Left Admin Side -->








<?php } ?>

<?php require '../components/footer.php' ?>