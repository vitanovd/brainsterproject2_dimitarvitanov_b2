<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Include font awsome style  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../public/css/style.css">
</head>

<body>
<div class="container">
<div class="row">
<div class="col-12">
</div>
</div>
</div>

    <div class="credentials d-flex justify-content-center align-items-center">
        <div class="credentials-container">
     
            <img src="img/login.jpg" alt="" class="credentials-image">
            <h2 class="text-center mt-2"> <i class="fa fa-user fa-2x"></i> </h2>
            <?php if(isset($_GET['error'])){
                echo "<div class='alert alert-danger text-center'> Wrong credentials ! </div>"; 
            }else{
                
                echo "<div class='alert alert-success text-center'> Please enter credentials ! </div>"; 
   
            }?>
            <form action="../../public/validation/loginvalidation.php" class="mt-4" method="post">
                <div class="form-row justify-content-center">
                    <div class="form-group col-10">
                        
                        <label for="name" class="text-muted font-weight-bold">Username:</label>
                        <input type="text" name="username" id="username" class="form-control" >
                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10">
                        <label for="password" class="text-muted font-weight-bold">Password:</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                </div>

                <div class="from-row">
                    <div class="form-group col-12">
                        <button type="submit" class="btn btn-outline-primary mt-2 float-right">Login</button>

                    </div>
                </div>
            </form>

            
        </div>
    

    </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>