<?php
session_start();
if (!isset($_SESSION['user']) && !isset($_SESSION['password'])) {
    header('Location:../../views/admin/login.php');
} else {
?>
    <?php include 'components/header.php';
    require 'components/nav.php';
    require '../../db.php';
    ?>


    <div class="col-12 col-lg-8 py-2 mx-auto">
        <div class="d-flex justify-content-between mt-4">
            <h4 class="">General Information </h4>
            <div class="text-right">
                <a class="text-dark  " href="../../views/admin/logout.php"><i class="fas fa-sign-out-alt mr-2"> </i> Logout |</a>
                <?php echo date('Y/m/d'); ?>
            </div>
        </div>
        <span class="border border-secondary d-block mt-2"></span>

        <h4 class="mt-4">Total cases worldwide </h4>


        <div class="row pt-2">
            <div class="col-12 col-md-6 col-xl-3 mt-2 ">
                <div class="card border-primary ">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Countries</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-danger"> </i> <span class="h3 ml-2"><?php $numCountries = $countryObj->getCountCountries();
                                                                                                                        echo $numCountries['numCountries']; ?> </span> </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-6 col-xl-3 mt-2 ">
                <div class="card border-primary">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Total confirmed</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-danger"> </i> <span class="h3 ml-2"><?php $confirmed = $casesObj->getTotalCases();
                                                                                                                        echo number_format($confirmed['TotalCases']) ?> </span> </p>
                    </div>
                </div>
            </div>



            <div class="col-12 col-md-6 col-xl-3 mt-2 ">
                <div class="card border-primary">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Total recovered</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-success"> </i> <span class="h3 ml-2"><?php $recovered = $casesObj->getTotalRecovered();
                                                                                                                        echo number_format($recovered['TotalCases']) ?> </span> </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-xl-3 mt-2  ">
                <div class="card border-primary">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Total deaths</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-danger"> </i> <span class="h3 ml-2"><?php $deaths = $casesObj->getTotalDeaths();
                                                                                                                        echo number_format($deaths['TotalCases']) ?> </span> </p>
                    </div>
                </div>
            </div>


        </div>

        <div class="row pt-2 px-0 ">
            <div class="col-12 col-xl-6">
                <h4 class="mt-4">Countries</h4>

                <span class="border border-secondary d-block mt-2"></span>
                <table id="tableToday" class="display table-responsive" style="height:400px">
                    <thead>
                        <tr>
                            <th>Country</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($countryObj->selectAllCountries() as $data) {   ?>
                            <tr>
                                <td><?= $data->name ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>


            <div class="col-12 col-lg-4 offset-0 offset-xl-1">
                <h4 class="mt-4">Update current data</h4>
                <?php
                if (isset($_GET['sync'])) {
                    if ($_GET['sync'] == true) {
                        if($casesObj->update()){
                            echo "<div class'alert alert-success'> Table updated </div>";
                        }
                        else{
                            echo "<div class='alert alert-danger'> Please try again in several minutes either app stopped working or everything is up to date </div>";
                        }
                    }
                }

                ?>
                <span class="border border-secondary d-block mt-2"></span>
                <a href="?sync=true" class='btn btn-warning mt-2'>Sync new data</a>
            </div>
        </div>
    </div> <!-- Left Admin Side -->








<?php } ?>

<?php require 'components/footer.php' ?>