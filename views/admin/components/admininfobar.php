<div class="row pt-2">
            <div class="col-12 col-md-6 col-xl-3 mt-2 ">
                <div class="card border-primary ">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Countries</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-danger"> </i> <span class="h3 ml-2"><?php $numCountries = $countryObj->getCountCountries();  echo $numCountries['numCountries'];?> </span> </p>
                    </div>
                </div>
            </div>


            <div class="col-12 col-md-6 col-xl-3 mt-2 ">
                <div class="card border-primary">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Total confirmed</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-danger"> </i> <span class="h3 ml-2"><?php $confirmed = $casesObj->getTotalCases(); echo number_format($confirmed['TotalCases'])?> </span> </p>
                    </div>
                </div>
            </div>



            <div class="col-12 col-md-6 col-xl-3 mt-2 ">
                <div class="card border-primary">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Total recovered</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-success"> </i> <span class="h3 ml-2"><?php $recovered = $casesObj->getTotalRecovered(); echo number_format($recovered['TotalCases'])?>  </span> </p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-xl-3 mt-2  ">
                <div class="card border-primary">
                    <div class="card-body">
                        <h2 class="card-heading text-center">Total deaths</h2>
                        <p class="text-center"> <i class="fa fa-users fa-3x text-danger"> </i> <span class="h3 ml-2"><?php $deaths = $casesObj->getTotalDeaths(); echo number_format($deaths['TotalCases'])?> </span> </p>
                    </div>
                </div>
            </div>


        </div>
