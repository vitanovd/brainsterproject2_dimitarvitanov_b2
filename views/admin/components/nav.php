        <div class="navigation ">
            <a class="navbar-brand admin-logo-link d-none d-lg-block mx-auto" href="#"> <img src="../../../public/img/logo.png" class='img-fluid admin-logo'></a>

            <nav class="navbar navbar-expand-lg navbar-light ">

                <button class="navbar-toggler ml-auto bg-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse flex-column justify-content-start align-items-start" id="navbarSupportedContent">
                    <ul class="nav nav-pills d-flex flex-column justify-content-end  ml-0 ml-lg-0 ml-xl-3  align-items-start">


                        <li class="nav-item mb-4">
                            <a class="nav-link " href="../../../index.php"><i class="fa fa-home mr-2"> </i> Homepage</a>
                        </li>

                        <li class="nav-item">
                            <?php file_exists('../admin/admin.php') ? print " <a class='nav-link ' href='../admin/admin.php'>Admin Home</a>" : print "<a class='nav-link ' href='../../admin/admin.php'>Admin Home</a>"  ; ?>

                        </li>

                        
                        <li class="nav-item">
                            <?php file_exists('pages/menu.php') ? print " <a class='nav-link ' href='pages/menu.php'>Menu</a>" : print "<a class='nav-link ' href='../pages/menu.php'> Menu</a>"  ; ?>
                        </li>


                        <li class="nav-item">
                            <?php file_exists('pages/about.php') ? print " <a class='nav-link ' href='pages/about.php'>About</a>" : print "<a class='nav-link ' href='../pages/about.php'> About</a>"  ; ?>
                        </li>
                   
                        <li class="nav-item">
                            <?php file_exists('pages/countries.php') ? print " <a class='nav-link ' href='pages/countries.php'>All Countries</a>" : print "<a class='nav-link ' href='../pages/countries.php'> All countries</a>"  ; ?>
                        </li>

                        <li class="nav-item">
                            <?php file_exists('pages/cases.php') ? print " <a class='nav-link ' href='pages/cases.php'>All cases</a>" : print "<a class='nav-link ' href='../pages/cases.php'> All cases</a>"  ; ?>
                        </li>
                        
            

                      

                    </ul>
                    <div class=" icons d-none d-md-flex  justify-content-center align-items-center mb-4">
                            <a href="#" class="fab fa-facebook-square fa-2x mx-2"></a>
                            <a href="#" class="fab fa-instagram  fa-2x mx-2"></a>
                            <a href="#" class="fab fa-linkedin fa-2x mx-2"></a>
                            <a href="#" class="fa fa-envelope-open    fa-2x mx-2"></a>

                        </div>

                </div>
            </nav> <!-- Navbar End -->
        </div>
        <!--Navigation End -->


    </div> <!-- End of vertical menu -->
