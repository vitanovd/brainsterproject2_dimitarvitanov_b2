<?php
if(!isset($_SESSION['lang']) && isset($_GET['lang']) || isset($_SESSION['lang']) && isset($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang'];
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Include font awsome style  -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

        <?php $cssCheck = file_exists('../../public/css/style.css')  ? print "<link rel='stylesheet' href='../../public/css/style.css'>" : print "<link rel='stylesheet' href='../../../public/css/style.css'>" ?> 
    </head>

    <body>

        <!-- Admin navigation -->

        <div class="row mx-0 py-0 my-0 ">
          <div class=" col-12 col-lg-4 col-xl-2 px-0  ">

       