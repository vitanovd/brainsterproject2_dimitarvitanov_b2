<?php include 'db.php'?>
<nav class="navbar navbar-expand-lg navbar-dark " data-aos="zoom-in">
        <button class="navbar-toggler ml-auto " type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon  "></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <a href="#" class="navbar-brand"><img src="public/img/logo.png" class="img-fluid logo float-right" alt=""></a>

            <ul class="navbar-nav ml-auto">
                <?php $menuData = $menuObj->selectAll();?>
                <?php   foreach($menuData as $menu) {?>
                <li class="nav-item ">
                    <a class="nav-link" href="<?=$menu->menu_link ?>"><?= $menu->menu_title ?> <span class="sr-only">(current)</span></a>
                </li>
                <?php }?>
            </ul>
        </div>
    </nav>