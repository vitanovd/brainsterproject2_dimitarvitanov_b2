<div class="container-fluid px-0  " data-aos="zoom-in" id="covidStatistics">
    <div class="row mx-0">

        <div class="col-12 col-lg-5 offset-lg-1 mt-5 ">
            <h2 class='text-center py-2 py-md-5 '> Overall Total Cases</h2>

            <table id="tableToday" class="display table-responsive">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Confirmed</th>
                        <th class="bg-success">Recovered</th>
                        <th class="bg-danger">Deaths</th>
                        <th class="bg-danger">Last Date</th>




                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($casesObj->getLastDataOnGlobalScale() as $case) {   ?>
                
                        <tr>
                      
                            <td><a href="../views/list/show.php?id=<?= $case->id ?>" class="text-muted"><?= $case->name ?>   </a></td>
                            <td><?= $case->confirmed ?></td>
                            <td class="bg-success"><?= $case->recovered ?></td>
                            <td class="bg-danger"><?= $case->deaths ?></td>
                            <td ><?= $case->date ?></td>


                        </tr>
                     
                    <?php } ?>
                </tbody>
            </table>


        </div>




        <div class="col-12 col-lg-5 mt-5" >
            <h2 class='text-center py-2 py-md-5'> Total Cases From Today</h2>
            <table id="tableToday2" class="display table-responsive">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Confirmed</th>
                        <th class="bg-success">Recovered</th>
                        <th class="bg-danger">Deaths</th>
                        <th class="bg-danger">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($casesObj->getLastUpdatedData() as $case) {   ?>
                        <tr>
                        <td><a href="../views/list/show.php?id=<?= $case->id ?>" class="text-muted"><?= $case->name ?>   </a></td>
                            <td><?= $case->confirmed_today ?></td>
                            <td class="bg-success"><?= $case->recovered_today ?></td>
                            <td class="bg-danger"><?= $case->death_today ?></td>
                            <td><?= $case->date ?></td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>


        </div>






    </div>
</div>


<div class="container"  >
    <div class="row justify-content-center align-items-center">
        <div class="col-4 offset-md-1 offset-lg-0 mt-5" >
        <div class="ct-chart ct-perfect-fourth"></div> 
        </div>


        <div class="col-6 offo col-lg-4 offset-lg-3 mt-5" >
                        <div class="d-flex align-items-center" >
                        <div class="box"></div>
                        <small>Total COVID-19 <span class='font-weight-bold'>death cases </span> worldwide</small>
                        </div>

                        <div class="d-flex align-items-center my-2">
                        <div class="box1"></div>
                        <small>Total COVID-19 <span class='font-weight-bold'>confirmed cases </span> worldwide</small>
                        </div>

                        <div class="d-flex align-items-center">
                        <div class="box2"></div>
                        <small>Total COVID-19 <span class='font-weight-bold'>recovered cases </span> worldwide</small>
                        </div>
                

         </div>

    </div>

</div>