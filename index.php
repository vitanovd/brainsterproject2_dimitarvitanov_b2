<?php require_once 'db.php' ?>
<?php require_once 'views/layouts/header.php' ?>
<?php require 'public/functions/functions.php' ?>

<?php
/* Get all Countries 
 getCountries();
*/

/* Get all Cases 
 getCases();
*/
/* Updating Cases based on new date


 */

// $casesObj->update();

?>

<div class="covid-image">
    <?php require_once 'views/navbar.php' ?>



    <div class="covid d-flex justify-content-center flex-column ">
        <div class="container">
            <div class="row">
                <div class="col-5">
                </div>
                <div class="col-12 col-lg-7" data-aos="zoom-in">
                <?php  $covidData = $covidObj->selectAll();  ?>
                    <?php foreach($covidData as $data) {?>
                    <h1 class=" text-white text-uppercase font-style-italic offset-0 offset-lg-3 px-3 px-lg-0 "><?= $data->about_covid_title ?></h1>
                    <div class="row">
                        <div class="col-12 col-lg-9 offset-0 offset-lg-3">
                            <h5 class="text-white mt-3 px-3 px-md-0"> <?= $data->about_covid ?></h5>
                            <a href="https://www.who.int/health-topics/coronavirus#tab=tab_1" class="btn btn-danger text-white mt-2 float-right mr-4">Learn More</a>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>

    </div>
</div>



<?php include 'views/infobar.php' ?>


<div class="container-fluid pb-5 pt-md-5 latest-figures d-flex" data-aos="zoom-in">
    <div class="row justify-content-center align-items-center">
    <?php foreach($covidData as $data) { ?>

        <div class="col-lg-6 mx-md-4 mt-md-5">
            <h2 class="display-4"><?= $data->article_title ?></h2>
            <p class="mt-4 mt-md-0">
              <?= $data->article_content?>
            </p>
        </div>

        <div class="col-2 d-flex justify-content-center">
            <img src="public/img/covidimage.png" class="d-none d-md-block img-fluid " alt="Covid Image">

        </div>

    <?php } ?>
    </div>
</div>

</div>

<div class="divider"></div>

<?php require_once 'views/covidstatistics.php' ?>

<div class="divider mt-5"></div>


<?php require 'views/optionpart.php'?>

<div class="divider mt-5"></div>

<!-- Map -->
<div class="container py-5" id="covidMap">
    <div class="row">
        <div class="col-10 offset-1">
            <h2 class="text-center py-5">Covid-19 Map</h2>
            <div id="regions_div"></div>
        </div>
    </div>
</div>






<?php require_once 'views/layouts/footer.php' ?>