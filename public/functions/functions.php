<?php
function getCountries()
{
    include 'db.php';
    $handle = curl_init();
    $url = "https://api.covid19api.com/countries";
    curl_setopt($handle, CURLOPT_URL, $url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($handle);
    curl_close($handle);
    $countries = json_decode($result, true);
    $i = 1;
    foreach ($countries as $country) {
        if ($i == 10) {
            $i = 1;
            echo "sleep ";
            set_time_limit(3000);
            sleep(60);
        }
        $handle = curl_init();
        $url = "https://api.covid19api.com/total/dayone/country/{$country['Slug']}";
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($handle);
        curl_close($handle);
        $cases = json_decode($result, true);
        if (empty($cases)) {
            $hasData = 'false';
        } else {
            $hasData = 'true ';
        }
        $name = addslashes($country['Country']);
        $country['Country'];
        $slug = $country['Slug'];
        $ISO2 = $country['ISO2'];
        $sql = "INSERT INTO countries(name, slug, ISO2, isEmptyData) VALUES ('{$name}', '{$slug}', '{$ISO2}', '{$hasData}')";
        $countryObj->insert($sql);
        $i++;
    }
}
function getCases()
{
    include 'db.php';
    $countries = $countryObj->selectAll('countries');
    foreach ($countries as $country) {
        $handle = curl_init();
        $url = "https://api.covid19api.com/total/dayone/country/{$country->slug}";
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($handle);
        curl_close($handle);
        $cases = json_decode($result, true);
        $i=1;
        $request = get_headers('https://api.covid19api.com/');
        if ($request[0] !== "HTTP/1.1 200 OK") {
            die();
        }
        foreach ($cases as $key => $case) {
  
            if ($i == 10) {
                $i = 1;
                echo "sleep ";
                set_time_limit(3000);
                sleep(60);
            }
            if ($key === 0) {
                $confirmed = $case['Confirmed'];
                $deaths = $case['Deaths'];
                $recovered = $case['Recovered'];
                $sql = "INSERT INTO cases (country_id, confirmed, deaths, recovered,active, date, confirmed_today, death_today, recovered_today)VALUES($country->id, $confirmed,$deaths,$recovered,{$case['Active']}, '{$case['Date']}',  $confirmed,$deaths,$recovered)";
                $casesObj->insert($sql);
            } else if ($key > 0) {
                if ($key == 'message') {
                    return;
                }
                $prevIndex = $cases[$key - 1];
                $todaycases = $case['Confirmed'] - $prevIndex['Confirmed'];
                $todaydeaths = $case['Deaths'] - $prevIndex['Deaths'];
                $todayrecovered = $case['Recovered'] - $prevIndex['Recovered'];
                $sql = "INSERT INTO cases (country_id, confirmed, deaths, recovered,active, date, confirmed_today, death_today, recovered_today)VALUES($country->id, {$case['Confirmed']}, {$case['Deaths']},{$case['Recovered']},{$case['Active']}, '{$case['Date']}',  $todaycases,$todaydeaths,$todayrecovered)";
                $casesObj->insert($sql);
            }
            $i++;
        }
    }
}
